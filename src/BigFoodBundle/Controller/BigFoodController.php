<?php

namespace BigFoodBundle\Controller;

use OrderBundle\Entity\Cart;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BigFoodController extends Controller
{
    /**
     * Return the main layout
     */
    public function indexAction()
    {
        return $this->render('BigFoodBundle::layout.html.twig');
    }

    /**
     * Return the count of recipes in the cart to display on the cart icon in navbar
     */
    public function countCartAction()
    {
        $em = $this->getDoctrine()->getManager();

        $count = 0;

        if($cart = $em->getRepository(Cart::class)->findOneBy(['owner' => $this->getUser(), 'status' => 1])) {
            $count = count($cart->getRecipes());
        }

        return $this->render('BigFoodBundle::cart.hmtl.twig', array(
           'count' => $count
        ));
    }
}
