<?php

namespace RestaurantBundle\Controller;

use RestaurantBundle\Entity\Recipe;
use RestaurantBundle\Entity\Restaurant;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class MenuController extends Controller
{
    /**
     * Return the view to display a restaurant's menu
     *
     * @param Restaurant $restaurant
     * @param $type
     */
    public function viewMenuAction(Restaurant $restaurant, $type)
    {
        $em = $this->getDoctrine()->getManager();

        if ($type == 'all'){
            $burgers = $em->getRepository(Recipe::class)->findRecipesByType('Burgers');
            $drinks = $em->getRepository(Recipe::class)->findRecipesByType('Drinks');
            $desserts = $em->getRepository(Recipe::class)->findRecipesByType('Desserts');
            return $this->render('RestaurantBundle::view_menu.html.twig', array(
                'restaurant' => $restaurant,
                'burgers'    => $burgers,
                'drinks'     => $drinks,
                'desserts'   => $desserts
            ));

        }else{
            $recipes = $em->getRepository(Recipe::class)->findRecipesByType($type);
            return $this->render('RestaurantBundle::view_menu.html.twig', array(
                'type'        => $type,
                'restaurant'  => $restaurant,
                'recipes'     => $recipes
            ));
        }
    }

    /**
     * Return the view where the manager can add or remove a recipe to the menu
     *
     * @Security("has_role('ROLE_MANAGER')")
     */
    public function manageMenuAction(Restaurant $restaurant)
    {
        if($restaurant->getManager()->getId() == $this->getUser()->getId()) {
            $em = $this->getDoctrine()->getManager();
            $recipes = $em->getRepository('RestaurantBundle:Recipe')->findBy([], ['type' => 'ASC']);

            return $this->render(
                'RestaurantBundle::manage_menu.html.twig',
                array(
                    'restaurant' => $restaurant,
                    'recipes' => $recipes
                )
            );
        } else {

            return new Response('Nope');

        }
    }

    /**
     * Adds a recipe to the restaurant's menu
     *
     * @Security("has_role('ROLE_MANAGER')")
     * @ParamConverter("restaurant", options={"mapping": {"restaurant": "id"}})
     * @ParamConverter("recipe", options={"mapping": {"recipe": "id"}})
     */
    public function addRecipeToMenuAction(Restaurant $restaurant, Recipe $recipe)
    {
        if($restaurant->getManager()->getId() == $this->getUser()->getId()) {
            $em = $this->getDoctrine()->getManager();
            $restaurant->addRecipe($recipe);
            $em->persist($restaurant);
            $em->flush();

            return $this->redirectToRoute('manage_menu',array(
                'id' => $restaurant->getId()
            ));
        }else {

            return new Response('Nope');
        }
    }

    /**
     * Removes a recipe from the restaurant's menu
     *
     * @Security("has_role('ROLE_MANAGER')")
     * @ParamConverter("restaurant", options={"mapping": {"restaurant": "id"}})
     * @ParamConverter("recipe", options={"mapping": {"recipe": "id"}})
     */
    public function removeRecipeFromMenuAction(Restaurant $restaurant, Recipe $recipe)
    {
        // If the user is the restaurant's manager
        if($restaurant->getManager()->getId() == $this->getUser()->getId()) {
            $em = $this->getDoctrine()->getManager();
            $restaurant->removeRecipe($recipe);
            $em->persist($restaurant);
            $em->flush();

            return $this->redirectToRoute('manage_menu', array(
                'id' => $restaurant->getId()
            ));
        }else {

            return new Response('nope');
        }
    }
}