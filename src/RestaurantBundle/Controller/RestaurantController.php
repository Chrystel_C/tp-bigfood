<?php

namespace RestaurantBundle\Controller;

use RestaurantBundle\Entity\Restaurant;
use RestaurantBundle\Form\RestaurantType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class RestaurantController extends Controller
{

    /**
     * Returns the list of restaurants to display in navbar
     */
    public function restaurantsListAction()
    {
        $em = $this->getDoctrine()->getManager();
        $restaurants = $em->getRepository('RestaurantBundle:Restaurant')->findAll();

        return $this->render('RestaurantBundle::restaurants_list.html.twig', array(
            'restaurants' => $restaurants
        ));
    }

    /**
     * Returns the view to create a new restaurant's page
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function createAction(Request $request)
    {
       $restaurant = new Restaurant();
       $form = $this->createForm(RestaurantType::class, $restaurant);

       $em = $this->getDoctrine()->getManager();

       if($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
           $em->persist($restaurant);
           $em->flush();

           $this->addFlash('success', 'Restaurant has been created.');

           return $this->redirectToRoute('view_restaurant', array(
               'id' => $restaurant->getId(),
               'name' => $restaurant->getName()
           ));
       }

       return $this->render('RestaurantBundle::create_restaurant.html.twig', array(
          'form' => $form->createView()
       ));
    }

    /**
     * Returns the view of a restaurant's page
     *
     * @param Restaurant $restaurant
     */
    public function viewAction(Restaurant $restaurant)
    {
        return $this->render('RestaurantBundle::view_restaurant.html.twig', array(
           'restaurant' => $restaurant
        ));
    }

    /**
     * Sets a manager to a restaurant
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function setManagerAction(Restaurant $restaurant, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('UserBundle:User')->findAll();

        if($request->isMethod('POST')) {

            $userId = $request->request->get('manager');

            $user = $em->getRepository('UserBundle:User')->find($userId);

            $restaurant->setManager($user);
            $user->addRole('ROLE_MANAGER');
            $em->persist($restaurant);
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Manager has been set');

            return $this->redirectToRoute('view_restaurant', array(
                'name'  => $restaurant->getName(),
                'id'    => $restaurant->getId()
            ));
        }

        return $this->render('RestaurantBundle::set_manager.html.twig', array(
           'restaurant' => $restaurant,
           'users' => $users
        ));
    }
}
