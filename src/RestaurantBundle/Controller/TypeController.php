<?php

namespace RestaurantBundle\Controller;

use RestaurantBundle\Entity\Type;
use RestaurantBundle\Form\TypeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class TypeController extends Controller
{
    /**
     * Creates a new type of recipe
     *
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function addRecipeTypeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $type = new Type();

        $form = $this->createForm(TypeType::class, $type);

        if($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->persist($type);
            $em->flush();

            $this->addFlash('success', 'Recipe type has been added.');

            return $this->redirectToRoute('homepage');
        }

        return $this->render('RestaurantBundle::add_recipe_type.html.twig', array(
           'form' => $form->createView()
        ));
    }
}