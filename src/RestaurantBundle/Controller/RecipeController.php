<?php

namespace RestaurantBundle\Controller;

use RestaurantBundle\Entity\Recipe;
use RestaurantBundle\Form\RecipeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class RecipeController extends Controller
{
    /**
     * Returns the view to create a new recipe
     *
     * @Security("has_role('ROLE_MANAGER')")
     */
    public function createRecipeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $recipe = new Recipe();

        $form = $this->createForm(RecipeType::class, $recipe);

        if($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {
            $em->persist($recipe);
            $em->flush();

            $this->addFlash('success', 'Recipe has been created.');

            return $this->redirectToRoute('homepage');
        }

        return $this->render('RestaurantBundle::create_recipe.html.twig', array(
           'form' => $form->createView()
        ));
    }

    /**
     * Returns the view of all recipes, where the manager can edit or delete a recipe
     *
     * @Security("has_role('ROLE_MANAGER')")
     */
    public function manageRecipeAction()
    {
        $em = $this->getDoctrine()->getManager();

        $recipes = $em->getRepository('RestaurantBundle:Recipe')->findBy([], ['type' => 'ASC']);

        return $this->render('RestaurantBundle::manage_recipe.html.twig', array(
            'recipes' => $recipes
        ));
    }

    /**
     * Returns the view to edit a recipe
     *
     * @Security("has_role('ROLE_MANAGER')")
     */
    public function editRecipeAction(Recipe $recipe, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(RecipeType::class, $recipe);

        if($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {

            $em->persist($recipe);
            $em->flush();

            $this->addFlash('success', 'Recipe saved');

            return $this->redirectToRoute('manage_recipe');
        }

        return $this->render('RestaurantBundle::edit_recipe.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Deletes a recipe
     *
     * @Security("has_role('ROLE_MANAGER')")
     */
    public function deleteRecipeAction(Recipe $recipe)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($recipe);
        $em->flush();

        $this->addFlash('danger', $recipe->getName() . ' deleted');

        return $this->redirectToRoute('manage_recipe');
    }
}
