<?php

namespace OrderBundle\Controller;

use OrderBundle\Entity\Cart;
use RestaurantBundle\Entity\Recipe;
use RestaurantBundle\Entity\Restaurant;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{
    /**
     * Return the view where the manager of a restaurant can see and manage orders
     *
     * @Security("has_role('ROLE_MANAGER')")
     */
    public function manageOrdersAction()
    {
        $em = $this->getDoctrine()->getManager();

        if($restaurant = $em->getRepository(Restaurant::class)->findOneBy(['manager' => $this->getUser()])) {

            $carts = $em->getRepository(Cart::class)->findBy(['status' => 2, 'restaurant' => $restaurant]);

            return $this->render('OrderBundle::manage_orders.html.twig', array(
                'restaurant'    => $restaurant,
                'carts'          => $carts
            ));
        } else {
            $this->addFlash('danger', 'You are not a manager of any restaurant');
            return $this->redirectToRoute('homepage');
        }
    }

    /**
     * Return the checkout view, display only carts in preparation by the client
     *
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     */
    public function checkoutAction()
    {
        $em = $this->getDoctrine()->getManager();

        if($cart = $em->getRepository(Cart::class)->findOneBy(['owner' => $this->getUser(), 'status' => 1])){

            return $this->render('OrderBundle::checkout.html.twig', array(
                'cart' => $cart
            ));
        } else {

            return $this->render('OrderBundle::checkout.html.twig');
        }
    }

    /**
     * Add a recipe in the cart. Create a cart if it doesnt exists already.
     *
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @ParamConverter("recipe", options={"mapping": {"recipe": "id"}})
     * @ParamConverter("restaurant", options={"mapping": {"restaurant": "id"}})
     */
    public function addRecipeToCartAction(Recipe $recipe, Restaurant $restaurant)
    {
        $em = $this->getDoctrine()->getManager();

        if (!($cart = $em->getRepository(Cart::class)->findOneBy(['owner' => $this->getUser(), 'status' => 1]))) {
            $cart = new Cart();
            $cart->setStatus(1);
            $cart->setOwner($this->getUser());
        }

        if ($cart->getRestaurant() != null) {

            if($cart->getRestaurant()->getId() != $restaurant->getId()){
                $this->addFlash('warning', 'You cannot add recipes from different Big Food shop in your cart at the same time');

                return $this->redirectToRoute('homepage');
            }

            if ($cart->getRestaurant()->hasRecipe($recipe)) {
                $cart->addRecipe($recipe);
            }
        }else{
            $cart->setRestaurant($restaurant);
            $cart->addRecipe($recipe);
        }

        $em->persist($cart);
        $em->flush();

        $this->addFlash('success', $recipe->getName() . ' added to your cart !');

        return $this->redirectToRoute('view_menu', array(
            'id'    => $restaurant->getId(),
            'type'  => 'all'
        ));
    }

    /**
     * Remove a recipe from the cart. Redirect and delete the cart if its empty
     *
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @ParamConverter("recipe", options={"mapping": {"recipe": "id"}})
     * @ParamConverter("cart", options={"mapping": {"cart": "id"}})
     */
    public function removeFromCartAction(Recipe $recipe, Cart $cart)
    {
        $em = $this->getDoctrine()->getManager();

        if($cart->getOwner()->getId() == $this->getUser()->getId()) {
            $cart->removeRecipe($recipe);

            if(count($cart->getRecipes()) == 0 ) {
                $em->remove($cart);
                $em->flush();

                $this->addFlash('success', $recipe->getName() . ' removed from cart');

                return $this->redirectToRoute('homepage');
            }

            $em->persist($cart);
            $em->flush();
        } else {

            return new Response('Nope');
        }

        $this->addFlash('success', $recipe->getName() . ' removed from cart');

        return $this->redirectToRoute('checkout');
    }

    /**
      * Transforms the cart in order.
      *
      * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
      */
    public function proceedWithCheckoutAction(Cart $cart)
    {
        $em = $this->getDoctrine()->getManager();
        $cart->setStatus(2);
        $cart->setDate(new \DateTime());

        $em->persist($cart);
        $em->flush();

        $this->addFlash('success', 'Your order will be ready in your restaurant in 30 minutes. Please come pick it up.');

        return $this->redirectToRoute('homepage');
    }

    /**
     * Archive an order if it has been picked up by the client
     *
     * @Security("has_role('ROLE_MANAGER')")
     */
    public function archiveOrderAction(Cart $cart)
    {
        if($cart->getRestaurant()->getManager()->getId() == $this->getUser()->getId()) {

            $cart->setStatus(3);
            $em = $this->getDoctrine()->getManager();
            $em->persist($cart);
            $em->flush();

            $this->addFlash('info', 'This order has been picked up.');

            return $this->redirectToRoute('manage_orders');
        } else {

            return new Response('Nope');
        }
    }
}
