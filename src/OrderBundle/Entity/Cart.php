<?php

namespace OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cart
 *
 * @ORM\Table(name="cart")
 * @ORM\Entity(repositoryClass="OrderBundle\Repository\CartRepository")
 */
class Cart
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", cascade={"persist"})
     */
    private $owner;

    /**
     * @ORM\ManyToMany(targetEntity="RestaurantBundle\Entity\Recipe", cascade={"persist"})
     */
    private $recipes;

    /**
     * @ORM\ManyToOne(targetEntity="RestaurantBundle\Entity\Restaurant", cascade={"persist"})
     */
    private $restaurant;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->recipes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set owner.
     *
     * @param \UserBundle\Entity\User|null $owner
     *
     * @return Cart
     */
    public function setOwner(\UserBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner.
     *
     * @return \UserBundle\Entity\User|null
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Add recipe.
     *
     * @param \RestaurantBundle\Entity\Recipe $recipe
     *
     * @return Cart
     */
    public function addRecipe(\RestaurantBundle\Entity\Recipe $recipe)
    {
        $this->recipes[] = $recipe;

        return $this;
    }

    /**
     * Remove recipe.
     *
     * @param \RestaurantBundle\Entity\Recipe $recipe
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeRecipe(\RestaurantBundle\Entity\Recipe $recipe)
    {
        return $this->recipes->removeElement($recipe);
    }

    /**
     * Get recipes.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecipes()
    {
        return $this->recipes;
    }

    /**
     * Set restaurant.
     *
     * @param \RestaurantBundle\Entity\Restaurant|null $restaurant
     *
     * @return Cart
     */
    public function setRestaurant(\RestaurantBundle\Entity\Restaurant $restaurant = null)
    {
        $this->restaurant = $restaurant;

        return $this;
    }

    /**
     * Get restaurant.
     *
     * @return \RestaurantBundle\Entity\Restaurant|null
     */
    public function getRestaurant()
    {
        return $this->restaurant;
    }

    /**
     * Set date.
     *
     * @param \DateTime|null $date
     *
     * @return Cart
     */
    public function setDate($date = null)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date.
     *
     * @return \DateTime|null
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set status.
     *
     * @param int $status
     *
     * @return Cart
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Return the total cost of the order.
     *
     * @return int
     */
    public function getTotalPrice()
    {
        $total = 0;

        foreach ($this->recipes as $recipe) {
            $total += $recipe->getPrice();
        }

        return $total;
    }
}
