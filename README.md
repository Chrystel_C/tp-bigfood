///////////////////////////////////////////////////////////////////////////////////////////

This is an evaluation project I made during my training.

//////////////////////////////////////////////////////////////////////////////////////////

**Big Food is a fictional restaurant's company.**

- Each restaurant has a page on the website. 
There's 3 restaurants at the moment but the administrator can create a new restaurant's page.

- Each restaurant has a manager, set by the administrator.

- A manager can create or delete recipes, and add or remove them from the restaurant's menu.

- Users can create an account on the website. 
While registering, they are informed that every order they make through the website has to be picked up.

- When they are logged in, they can add recipes to their cart.

- When they proceed to checkout, the cart becomes an order.
There is no online payment yet. Users have to come and pay to retrieve orders.

- Managers can see orders for their restaurant in the administration panel.

- Users can see past orders in their profile.




