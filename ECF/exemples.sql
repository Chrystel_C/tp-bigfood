CREATE TABLE `restaurant` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `town` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zipcode` int(11) NOT NULL,
  `manager_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `restaurant` (`id`, `name`, `town`, `address`, `zipcode`, `manager_id`) VALUES
(1, 'Big Food Comédie', 'Montpellier', '50 place de la Comédie', 34000, 2);

SELECT * FROM `restaurant` JOIN `user` ON `restaurant`.`manager_id` = `user`.`id` WHERE `user`.`username` = 'Haingo';